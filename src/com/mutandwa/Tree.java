package com.mutandwa;

import java.util.Comparator;

public class Tree {

    public static final Comparator<Tree> BY_FREQUENCY = new ByFrequency();

    private Node root;

    public Tree(Node root) {
        this.root = root;
    }

    @Override
    public String toString() {
        if (root != null) {
            return root.character + " => " + root.frequency;
        }
        return "";
    }

    public Node getRoot() { return root; }

    private static class ByFrequency implements Comparator<Tree> {

        @Override
        public int compare(Tree a, Tree b) {
            if (a == null || b == null) throw new NullPointerException();
            int freqOne = a.root.frequency;
            int freqTwo = b.root.frequency;
            if (freqOne > freqTwo) return 1;
            if (freqOne < freqTwo) return -1;
            return 0;
        }
    }
}


