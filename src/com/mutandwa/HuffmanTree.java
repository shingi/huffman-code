package com.mutandwa;

import edu.princeton.cs.algs4.MinPQ;
import edu.princeton.cs.algs4.StdOut;

import java.util.*;

public class HuffmanTree {

    private HashMap<Character, Integer> frequencies;
    private List<Tree> trees;
    private MinPQ<Tree> pq;

    public HuffmanTree(String message) {
        if (message == null || message.isEmpty()) throw new NullPointerException();
        // generate frequency table
        createFrequencyTable(message);
        trees = new ArrayList<>(frequencies.size());
        pq = new MinPQ<>(trees.size(), Tree.BY_FREQUENCY);
        createTrees();
        generateHuffmanTree();
    }

    private void createFrequencyTable(String message) {
        char[] chars = message.toCharArray();
        frequencies = new HashMap<>();
        for (Character ch : chars) {
            if (frequencies.containsKey(ch)) {
                Integer v = frequencies.get(ch);
                frequencies.put(ch, ++v);
            } else {
                frequencies.put(ch, 1);
            }
        }
    }

    private void createTrees() {
        for (Map.Entry<Character, Integer> entry : frequencies.entrySet()) {
            Tree tree = new Tree(new Node(entry.getKey(), entry.getValue()));
            trees.add(tree);
            pq.insert(tree);
        }
    }

    private void generateHuffmanTree() {
        if (pq.size() == 1) return;
        // remove two trees from priority queue, and make them into children of a new node
        Tree left = pq.delMin();
        Tree right = pq.delMin();
        int totalFreq = left.getRoot().frequency + right.getRoot().frequency;
        Node node = new Node();
        node.frequency = totalFreq;
        node.leftChild = left.getRoot();
        node.rightChild = right.getRoot();
        // insert this new three-node tree back into the priority queue
        pq.insert(new Tree(node));
        // keep repeating the above until there is only one tree left in the queue
        generateHuffmanTree();
    }

    public void printFrequencies() {
        StdOut.println(pq.min());
        Iterator it = frequencies.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());
        }
    }
}
