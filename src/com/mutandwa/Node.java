package com.mutandwa;

public class Node {
    Character character;
    int frequency;
    Node leftChild;
    Node rightChild;

    public Node() {}

    public Node(Character character, int frequency) {
        this.character = character;
        this.frequency = frequency;
    }
}

